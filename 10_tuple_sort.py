def tuple_sort(tpl):                    #tuple(sorted(nosort))
    sm = list(tpl)
    for i in range(0, len(sm)-1):
        for d in range(i+1, len(sm)):
            if sm[i]> sm[d]:
                a:int = sm[i]
                sm[i] = sm[d]
                sm[d] = a
    return tuple(sm)


nosort: tuple = (1, 5, 2, 3, 13, 20, 40, 11, 4)
sort = tuple_sort(nosort)
print(sort)