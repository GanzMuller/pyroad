def date(d, m, y):
    if 0 < m < 13 and 0 < d < 32 and y > 0:
        if m in [2] and y % 4 == 0 and d < 30:
            return 1
        elif m in [2] and y % 4 != 0 and d < 29:
            return 1
        elif m in [4, 6, 7, 9, 11] and d < 30:
            return 1
        elif m in [1, 3, 5, 7, 8, 10, 12]:
            return 1
        else:
            return 0
    else:
        return 0

dd = int(input("Введите день\n"))
mm = int(input("Введите месяц\n"))
yyyy = int(input("Введите год\n"))

print(bool(date(dd, mm, yyyy)))