import random

def list_to_set(a):
    a = set(a)
    return a

sm: list = []
random.seed(random.randint(1,2000))
for i in range(1,random.randint(3, 100)):
    sm.append(random.randint(1,15000))
print(f"****В виде списка****\n{sm}")

sm = list_to_set(sm)
print(f"****В виде множества****\n{type(sm)} ->\n{sm}")