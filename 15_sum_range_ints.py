def sum_range(a, z):
    sum: int = 0
    for i in range(a, z+1):
        sum += i
    return sum

i: int = 0

print("Программа суммирует все числа в диапазоне от 'a' до 'z'\n*****")
start = int(input("Введите число 'a'\n"))
end = int(input("Введите число 'z'\n"))

print(f"Сумма чисел = {sum_range(start,end)}")