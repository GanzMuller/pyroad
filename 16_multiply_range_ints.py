def sum_range(a, z):
    sum: int = 0
    for i in range(a, z+1):
        if i == a:
            sum += i
        else:
            sum *= i
    return sum

print("Программа перемножает все числа в диапазоне от 'a' до 'z'\n*****")
start = int(input("Введите число 'a'\n"))
end = int(input("Введите число 'z'\n"))

print(f"Умножив все числа получаем = {sum_range(start,end)}")