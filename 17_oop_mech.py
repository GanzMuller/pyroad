from time import sleep

class Mech:
    def __init__(self, name: str, ammo: int = 0):
        self.name: str = name
        self.ammo: int = ammo

    def ReloadMechsGun(self, x: int):
        if x == 500:
            print(f"Guns of {self.name} is full")
        else:
            x += 100
            print(f"Ammo filled, magazine: {x}")
        return x
    
    def __str__(self):
        return f"Status of {self.name}: {self.ammo} ammo"

class EliteMech(Mech):
    group = "mech of special forces"
    def Airstrike(self):
        print("A lot of rockets falling on enemy's units\n Congratulations, you won, Captain")

    def __str__(self):
        return f"{self.name} {self.group} arrived: {self.ammo} ammo"

mech1 = Mech("MechDroidXII", 100)
i: chr = 'a'
print(mech1)
while i != 'q':
    i = input("Do you want some ammo?(y/q)\n")
    if i == 'y':
        mech1.ammo = mech1.ReloadMechsGun(mech1.ammo)
    elif i == 'q':
        print("\nArmorer - \nHave a nice dayd!")
sleep(1)
print(f"\nAdmin -\nNetwork is down, we couldn't call to pilot of {mech1.name}!")
sleep(1)
print(f"\nOperator -\n{mech1.name} are down, need support!")

a = input("Do you want continue battle? (y/any)")
if a == 'y' or a == 'Y':
    print("Calling mechs operator of: ")
    mech2 = EliteMech(input(), 500)
    print(mech2)
    sleep(1)
    mech2.Airstrike()
else:
    print("Captain, we lose")