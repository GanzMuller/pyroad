class Astronaut:
    trip = "Earth-Mars"
    def __init__(self, pos: str, first_name: str, last_name: str):
        self.pos = pos
        self.first_name = first_name
        self.last_name = last_name
    
    def __str__(self):
        return f"Astronaut {self.first_name} {self.last_name} in query {self.pos}"

    def ready(self, spaceship):
        print(f"{self.first_name} {self.last_name}, pos: {self.pos}, spaceship: {spaceship} - ready to start")

Person1 = Astronaut(1, "Maxim", "Amosov")
spaceship = "X-ll1"
print(Person1)
Person1.ready(spaceship)
print("Good luck")