def dec_countfunc(func):
    def wrapper():
        wrapper.dec_count += 1
        print(wrapper.dec_count)
        return func()
    wrapper.dec_count = 0
    return wrapper

@dec_countfunc
def test():
    print('bla bla bla\n')

for i in range(1, int(input("Сколько раз вы хотите выполнить функцию?\n")) + 1):
    test()