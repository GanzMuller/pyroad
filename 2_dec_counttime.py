from time import monotonic,sleep

def dec_counttime(func):
    def wrapper(*args):
        wrapper.time_start = monotonic()
        func(*args)
        print(monotonic() - wrapper.time_start)
    return wrapper

@dec_counttime
def bigbrainprocess(sec):
    5 + 5
    sleep(sec)
    10 - 20
    (109010 + 102300)/11

bigbrainprocess(int(input("Сколько на подумать?\n")))