def no():
    print("Это не палиндром.")

enter: str = input("Введите входную строку для проверки на палиндромность:\n\n")
enter = enter.replace(' ', '', len(enter))
length = (len(enter)) 
check: int = 0

if length>2 and (length%2)==1:
    for i in range(0, int(length/2)):
        if enter[i] == enter[(length-1)-i]:
            check += 1
        else:
            no()
            break            
else:
    no()

if check == (int(length/2)):
    print("Да, это палиндром.")