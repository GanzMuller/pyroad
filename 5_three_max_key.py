import random

random.seed(random.randint(1, 10000))
my_dict = {i: random.randint(1, 1000) for i in range(1, 21)}
print(my_dict)
print(sorted(my_dict, key=my_dict.get)[-3:])