def dicts_join(a,b): #di1.update(di2)
    for key in b:
        if key in a and key in b:
            a[key] = a[key], b[key]
        else:
            a[key] = b.get(key)
    return a

di1 = {
    "v1": 200,
    "v2": 300,
    "v3": 400,
    "v4": 100,
    "v10": 305
    }
di2 = {
    "v10": 205,
    "v20": 304,
    "v30": 403,
    "v40": 102,
    }

joins = dicts_join(di1, di2)
print(joins)    