import random

def reverse_my_list(lst):
    a = lst[0]
    lst[0] = lst[len(lst)-1]
    lst[len(lst)-1] = a
    return lst

sm: list = []
random.seed(random.randint(1, 10000))
for i in range(1,10):
    sm.append(random.randint(1,20))

print(f"До: {sm}")

sm = reverse_my_list(sm)

print(f"После: {sm}")