import random

def to_reverse_tuple(lst):
    lst.reverse()
    b = tuple(lst)
    return b

random.seed(random.randint(1,10000))
sm: list = []
for i in range(1, random.randint(1,100)):
    sm.append(random.randint(1, 100))
print(sm)

ms = to_reverse_tuple(sm)
print(f"{type(ms)} - {ms}")
